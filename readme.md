## Synopsis

This was my first Alexa Skill.  My son and I wrote it so he could ask Alexa to tell him Hoagie (a frog) stories.

## Code Example

This is poorly written Python.  I recommend using it only to learn what not to do.  I don't know what I am doing in python yet..

## Motivation

We bought an Alexa and wanted to play around with Skills.  What better way to play than to create a skill my 3 year old can interact with

## Installation

The only addition you need is a mysql database with a single table called stories.  A key column and the story column.  I stored the user, password, database, and server address in an environment variable called hoagiedb=user,password,database,server.  This is no way the most secure way, but this isn't an app requring security.

## API Reference


## Tests


## Contributors

## License

Feel free to copy and paste!