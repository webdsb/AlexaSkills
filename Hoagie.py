__author__ = 'webdsb'

import logging
import os
from flaskext.mysql import MySQL
from flask import Flask, render_template
from flask_ask import Ask, statement, question

mysql = MySQL()
dbinfo = str(os.environ['hoagiedb'])
dbDataSplit = dbinfo.strip().split(',')

app = Flask(__name__)
app.config.from_object(__name__)
app.config['MYSQL_DATABASE_USER'] = dbDataSplit[0]
app.config['MYSQL_DATABASE_PASSWORD'] = dbDataSplit[1]
app.config['MYSQL_DATABASE_DB'] = dbDataSplit[2]
app.config['MYSQL_DATABASE_HOST'] = dbDataSplit[3]
mysql.init_app(app)

ask = Ask(app, "/")



logging.getLogger("flask_ask").setLevel(logging.DEBUG)

def new_conference():
    welcome_msg = render_template('welcome')
    return question(welcome_msg)

@ask.intent("HoagieIntent")
def ask_if_shot(story):
    story = story
    query = "SELECT story FROM stories WHERE keyid LIKE \'%" + story + "\%';"
    cursor = mysql.connect().cursor()

    cursor.execute("SELECT story FROM stories WHERE keyid LIKE \'%" + story + "%\';")
        
    data = cursor.fetchone()
    print ("Data is: ", cursor.rowcount)
    print ("Query is: ", query)
    if data is None:
        message = "I Sorry Matthew, I do not know that hoagie story.  Can you tell me?"    
    elif cursor.rowcount == 0:
        message = "Sorry Matthew, I do not know that hoagie story.  Can you tell me?" 
    else:
        story = data[0];
        message = story
  
    return statement(message)

if __name__ == '__main__':
    app.run(debug=True, port=8081)

